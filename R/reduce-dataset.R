## script di analisi dei mancanti a supporto di eventuali riduzioni del dataset

load("wind.RData")
Ana <- read.csv("stazioni.csv",header = F)

## calcola dati validi orari
hTime <- Dat$time
hValid <- !is.na(Dat[,-1])

## calcola dati validi triorari
## almeno un dato valido ogni 3h
h3 <- cut(as.numeric(format(hTime,format="%H")),
          breaks = (0:8)*3,
          right = F,
          labels = sprintf((0:7)*3,fmt="%02i"))
h3Time <- paste(format(hTime,format="%Y%m%d"),h3,sep="")
sum3h <- function(x) {tapply(x,h3Time,sum)}
h3Valid <- apply(hValid, MARGIN = 2, FUN = sum3h)
h3Valid <- h3Valid>0

## calcola giornate valide
## come giornate con almeno 6 3h valide
dTime <- substr(rownames(h3Valid),1,8)
sumd <- function(x) {tapply(x,dTime,sum)}
dValid <- apply(h3Valid, MARGIN = 2, FUN = sumd)
dValid <- dValid>6

## calcola mesi validi
## come mesi con almeno 20 giorni validi
mTime <- substr(rownames(dValid),1,6)
summ <- function(x) {tapply(x,mTime,sum)}
mValid <- apply(dValid, MARGIN = 2, FUN = summ)
mValid <- mValid>20

## calcola anni validi
## come anni con almeno 9 mesi validi
yTime <- substr(rownames(mValid),1,4)
sumy <- function(x) {tapply(x,yTime,sum)}
yValid <- apply(mValid, MARGIN = 2, FUN = sumy)
yValid <- yValid>9

## plot
yV <- yValid[,(1:(ncol(yValid)/2))*2]
idx <- match(substr(colnames(yV),3,10),Ana[,1])
for (i in 1:(nrow(yV)-1)) {
  ny <- nrow(yV)-i+1
  valid <- as.numeric(colSums(yV[i:nrow(yV),]) == ny)
  plot(Ana[idx,2],Ana[idx,3],cex=valid,main=rownames(yV)[i])
  text(Ana[idx,2],Ana[idx,3],labels = Ana[idx,5],cex = 0.5, col=valid)
}


